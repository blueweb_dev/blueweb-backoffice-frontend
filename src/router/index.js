import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
  routes: [

    // 회원
    {
      path: '/member/member',
      name: '회원관리',
      component: () => import('../AdminPages/Member/Member.vue'),
    },
    {
      path: '/member/grade',
      name: '등급관리',
      component: () => import('../AdminPages/Member/Grade.vue'),
    },

    // 도메인
    {
      path: '/domain/domain/order',
      name: '주문관리',
      component: () => import('../AdminPages/Domain/Domain/Order.vue'),
    },
    {
      path: '/domain/domain/product',
      name: '상품관리',
      component: () => import('../AdminPages/Domain/Domain/Product.vue'),
    },
    {
      path: '/domain/domain/policy',
      name: '상품관리',
      component: () => import('../AdminPages/Domain/Domain/Policy.vue'),
    },
    {
      path: '/domain/add/order',
      name: '주문관리',
      component: () => import('../AdminPages/Domain/Add/Order.vue'),
    },
    {
      path: '/domain/add/product',
      name: '상품관리',
      component: () => import('../AdminPages/Domain/Add/Product.vue'),
    },

    // 호스팅
    {
      path: '/hosting/web/order',
      name: '주문관리',
      component: () => import('../AdminPages/Hosting/Web/Order.vue'),
    },
    {
      path: '/hosting/web/product',
      name: '상품관리',
      component: () => import('../AdminPages/Hosting/Web/Product.vue'),
    },
    {
      path: '/hosting/web/server',
      name: '서버관리',
      component: () => import('../AdminPages/Hosting/Web/Server.vue'),
    },
    {
      path: '/hosting/server/order',
      name: '주문관리',
      component: () => import('../AdminPages/Hosting/Server/Order.vue'),
    },
    {
      path: '/hosting/server/product',
      name: '상품관리',
      component: () => import('../AdminPages/Hosting/Server/Product.vue'),
    },
    {
      path: '/hosting/server/assets',
      name: '자산관리',
      component: () => import('../AdminPages/Hosting/Server/Assets.vue'),
    },
    {
      path: '/hosting/colocation/order',
      name: '주문관리',
      component: () => import('../AdminPages/Hosting/Colocation/Order.vue'),
    },
    {
      path: '/hosting/colocation/unit',
      name: '유닛형 상품관리',
      component: () => import('../AdminPages/Hosting/Colocation/Unit.vue'),
    },
    {
      path: '/hosting/colocation/div',
      name: '구분형 상품관리',
      component: () => import('../AdminPages/Hosting/Colocation/Div.vue'),
    },
    {
      path: '/hosting/ssl/order',
      name: '주문관리',
      component: () => import('../AdminPages/Hosting/Ssl/Order.vue'),
    },
    {
      path: '/hosting/ssl/product',
      name: '상품관리',
      component: () => import('../AdminPages/Hosting/Ssl/Product.vue'),
    },
    {
      path: '/hosting/add/order',
      name: '주문관리',
      component: () => import('../AdminPages/Hosting/Add/Order.vue'),
    },
    {
      path: '/hosting/add/product',
      name: '상품관리',
      component: () => import('../AdminPages/Hosting/Add/Product.vue'),
    },


    // Dashboards
    {
      path: '/',
      name: 'analytics',
      component: () => import('../DemoPages/Dashboards/Analytics.vue'),
    },
    {
      path: '/dashboards/advertisement',
      name: 'advertisement',
      component: () => import('../DemoPages/Dashboards/Advertisement.vue'),
    },
    {
      path: '/dashboards/management',
      name: 'management',
      component: () => import('../DemoPages/Dashboards/Management.vue'),
    },
    {
      path: '/dashboards/helpdesk',
      name: 'helpdesk',
      component: () => import('../DemoPages/Dashboards/Helpdesk.vue'),
    },
    {
      path: '/dashboards/monitoring',
      name: 'monitoring',
      component: () => import('../DemoPages/Dashboards/Monitoring.vue'),
    },
    {
      path: '/dashboards/crypto',
      name: 'crypto',
      component: () => import('../DemoPages/Dashboards/Crypto.vue'),
    },
    {
      path: '/dashboards/project-management',
      name: 'project-management',
      component: () => import('../DemoPages/Dashboards/ProjectManagement.vue'),
    },
    {
      path: '/dashboards/product',
      name: 'product',
      component: () => import('../DemoPages/Dashboards/Product.vue'),
    },
    {
      path: '/dashboards/statistics',
      name: 'statistics',
      component: () => import('../DemoPages/Dashboards/Statistics.vue'),
    },

    // Applications

    {
      path: '/apps/mailbox',
      name: 'mailbox',
      meta: {layout: 'apps'},
      component: () => import('../DemoPages/Applications/Mailbox.vue'),
    },
    {
      path: '/apps/chat',
      name: 'chat',
      meta: {layout: 'apps'},
      component: () => import('../DemoPages/Applications/Chat.vue'),
    },
    {
      path: '/apps/forums-listing',
      name: 'forums-listing',
      component: () => import('../DemoPages/Applications/ForumsListing.vue'),
    },
    {
      path: '/apps/forums-threads',
      name: 'forums-threads',
      component: () => import('../DemoPages/Applications/ForumsThreads.vue'),
    },
    {
      path: '/apps/forums-discussion',
      name: 'forums-discussion',
      component: () => import('../DemoPages/Applications/ForumsDiscussion.vue'),
    },

    // Pages

    {
      path: '/pages/login',
      name: 'login',
      meta: {layout: 'userpages'},
      component: () => import('../DemoPages/UserPages/Login.vue'),
    },
    {
      path: '/pages/login-boxed',
      name: 'login-boxed',
      meta: {layout: 'userpages'},
      component: () => import('../DemoPages/UserPages/LoginBoxed.vue'),
    },
    {
      path: '/pages/register',
      name: 'register',
      meta: {layout: 'userpages'},
      component: () => import('../DemoPages/UserPages/Register.vue'),
    },
    {
      path: '/pages/register-boxed',
      name: 'register-boxed',
      meta: {layout: 'userpages'},
      component: () => import('../DemoPages/UserPages/RegisterBoxed.vue'),
    },
    {
      path: '/pages/forgot-password',
      name: 'forgot-password',
      meta: {layout: 'userpages'},
      component: () => import('../DemoPages/UserPages/ForgotPassword.vue'),
    },
    {
      path: '/pages/forgot-password-boxed',
      name: 'forgot-password-boxed',
      meta: {layout: 'userpages'},
      component: () => import('../DemoPages/UserPages/ForgotPasswordBoxed.vue'),
    },

    // Elements

    {
      path: '/elements/buttons-standard',
      name: 'buttons-standard',
      component: () => import('../DemoPages/Elements/Buttons/Standard.vue'),
    },
    {
      path: '/elements/buttons-pills',
      name: 'buttons-pills',
      component: () => import('../DemoPages/Elements/Buttons/Pills.vue'),
    },
    {
      path: '/elements/buttons-square',
      name: 'buttons-square',
      component: () => import('../DemoPages/Elements/Buttons/Square.vue'),
    },
    {
      path: '/elements/buttons-shadow',
      name: 'buttons-shadow',
      component: () => import('../DemoPages/Elements/Buttons/Shadow.vue'),
    },
    {
      path: '/elements/buttons-icons',
      name: 'buttons-icons',
      component: () => import('../DemoPages/Elements/Buttons/Icons.vue'),
    },
    {
      path: '/elements/dropdowns',
      name: 'dropdowns',
      component: () => import('../DemoPages/Elements/Dropdowns.vue'),
    },
    {
      path: '/elements/icons',
      name: 'icons',
      component: () => import('../DemoPages/Elements/Icons.vue'),
    },
    {
      path: '/elements/badges-labels',
      name: 'badges',
      component: () => import('../DemoPages/Elements/Badges.vue'),
    },
    {
      path: '/elements/cards',
      name: 'cards',
      component: () => import('../DemoPages/Elements/Cards.vue'),
    },
    {
      path: '/elements/loaders',
      name: 'loaders',
      component: () => import('../DemoPages/Elements/LoadingIndicators.vue'),
    },
    {
      path: '/elements/list-group',
      name: 'list-group',
      component: () => import('../DemoPages/Elements/ListGroups.vue'),
    },
    {
      path: '/elements/navigation',
      name: 'navigation',
      component: () => import('../DemoPages/Elements/NavigationMenus.vue'),
    },
    {
      path: '/elements/timelines',
      name: 'timeline',
      component: () => import('../DemoPages/Elements/Timeline.vue'),
    },
    {
      path: '/elements/utilities',
      name: 'utilities',
      component: () => import('../DemoPages/Elements/Utilities.vue'),
    },

    // Components

    {
      path: '/components/tabs',
      name: 'tabs',
      component: () => import('../DemoPages/Components/Tabs.vue'),
    },
    {
      path: '/components/accordions',
      name: 'accordions',
      component: () => import('../DemoPages/Components/Accordions.vue'),
    },
    {
      path: '/components/notifications',
      name: 'notifications',
      component: () => import('../DemoPages/Components/Notifications.vue'),
    },
    {
      path: '/components/swal-alerts',
      name: 'notifications-swal',
      component: () => import('../DemoPages/Components/SwalAlerts.vue'),
    },
    {
      path: '/components/modals',
      name: 'modals',
      component: () => import('../DemoPages/Components/Modals.vue'),
    },
    {
      path: '/components/progress-bar',
      name: 'progress-bar',
      component: () => import('../DemoPages/Components/ProgressBar.vue'),
    },
    {
      path: '/components/tooltips-popovers',
      name: 'tooltips-popovers',
      component: () => import('../DemoPages/Components/TooltipsPopovers.vue'),
    },
    {
      path: '/components/carousel',
      name: 'carousel',
      component: () => import('../DemoPages/Components/Carousel.vue'),
    },
    {
      path: '/components/calendar',
      name: 'calendar',
      component: () => import('../DemoPages/Components/Calendar.vue'),
    },
    {
      path: '/components/pagination',
      name: 'pagination',
      component: () => import('../DemoPages/Components/Pagination.vue'),
    },
    {
      path: '/components/count-up',
      name: 'count-up',
      component: () => import('../DemoPages/Components/CountUp.vue'),
    },
    {
      path: '/components/scrollable-elements',
      name: 'scrollable-elements',
      component: () => import('../DemoPages/Components/Scrollable.vue'),
    },
    {
      path: '/components/ratings',
      name: 'ratings',
      component: () => import('../DemoPages/Components/Ratings.vue'),
    },
    {
      path: '/components/image-crop',
      name: 'image-crop',
      component: () => import('../DemoPages/Components/ImageCrop.vue'),
    },

    // Tables

    {
      path: '/tables/regular-tables',
      name: 'regular-tables',
      component: () => import('../DemoPages/Tables/RegularTables.vue'),
    },
    {
      path: '/tables/dynamic-tables',
      name: 'dynamic-tables',
      component: () => import('../DemoPages/Tables/DynamicTables.vue'),
    },

    // Dashboard Widgets

    {
      path: '/widgets/chart-boxes',
      name: 'chart-boxes',
      component: () => import('../DemoPages/Widgets/ChartBoxes1.vue'),
    },
    {
      path: '/widgets/chart-boxes-2',
      name: 'chart-boxes-2',
      component: () => import('../DemoPages/Widgets/ChartBoxes2.vue'),
    },
    {
      path: '/widgets/chart-boxes-3',
      name: 'chart-boxes-3',
      component: () => import('../DemoPages/Widgets/ChartBoxes3.vue'),
    },
    {
      path: '/widgets/profile-boxes',
      name: 'profile-boxes',
      component: () => import('../DemoPages/Widgets/ProfileBoxes.vue'),
    },

    // Forms

    {
      path: '/forms/controls',
      name: 'controls',
      component: () => import('../DemoPages/Forms/Elements/Controls.vue'),
    },
    {
      path: '/forms/layouts',
      name: 'layouts',
      component: () => import('../DemoPages/Forms/Elements/Layouts.vue'),
    },
    {
      path: '/forms/validation',
      name: 'validation',
      component: () => import('../DemoPages/Forms/Elements/Validation.vue'),
    },
    {
      path: '/forms/wizards',
      name: 'wizards',
      component: () => import('../DemoPages/Forms/Elements/Wizards.vue'),
    },
    {
      path: '/forms/sticky-headers',
      name: 'sticky-headers',
      component: () => import('../DemoPages/Forms/Elements/Sticky.vue'),
    },
    {
      path: '/forms/datepicker',
      name: 'datepicker',
      component: () => import('../DemoPages/Forms/Components/Datepicker.vue'),
    },
    {
      path: '/forms/range-slider',
      name: 'range-slider',
      component: () => import('../DemoPages/Forms/Components/RangeSlider.vue'),
    },
    {
      path: '/forms/input-selects',
      name: 'input-selects',
      component: () => import('../DemoPages/Forms/Components/InputSelects.vue'),
    },
    {
      path: '/forms/toggle-switch',
      name: 'toggle-switch',
      component: () => import('../DemoPages/Forms/Components/ToggleSwitch.vue'),
    },
    {
      path: '/forms/wysiwyg-editors',
      name: 'editors',
      component: () => import('../DemoPages/Forms/Components/Editors.vue'),
    },
    {
      path: '/forms/input-mask',
      name: 'input-mask',
      component: () => import('../DemoPages/Forms/Components/InputMask.vue'),
    },
    {
      path: '/forms/clipboard',
      name: 'clipboard',
      component: () => import('../DemoPages/Forms/Components/Clipboard.vue'),
    },
    {
      path: '/forms/textarea-autosize',
      name: 'textarea-autosize',
      component: () => import('../DemoPages/Forms/Components/TextareaAutosize.vue'),
    },
    {
      path: '/forms/colorpicker',
      name: 'colorpicker',
      component: () => import('../DemoPages/Forms/Components/Colorpicker.vue'),
    },
    {
      path: '/forms/dropzone',
      name: 'dropzone',
      component: () => import('../DemoPages/Forms/Components/Dropzone.vue'),
    },

    // Charts

    {
      path: '/charts/chartjs',
      name: 'chartjs',
      component: () => import('../DemoPages/Charts/Chartjs.vue'),
    },
    {
      path: '/charts/apexcharts',
      name: 'apexcharts',
      component: () => import('../DemoPages/Charts/ApexCharts.vue'),
    },
    {
      path: '/charts/gauges',
      name: 'gauges',
      component: () => import('../DemoPages/Charts/Gauges.vue'),
    },
    {
      path: '/charts/sparklines',
      name: 'sparklines',
      component: () => import('../DemoPages/Charts/Sparklines.vue'),
    },

  ],
})
