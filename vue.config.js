module.exports = {
    runtimeCompiler: true,
    productionSourceMap: false,

    publicPath: process.env.NODE_ENV === 'production'
        ? './'
        : '/',
    devServer: {
        host: '0.0.0.0',
        port: 3000,
        disableHostCheck: true,
        overlay: {
            warnings: true,
            errors: true,
        },
    },
}